﻿namespace ToolSetSystem_Statistics
{
    partial class DBInfoPrompt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TB_Host = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TB_Port = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TB_Pass = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TB_User = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TB_DB = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button1.Location = new System.Drawing.Point(0, 680);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(574, 49);
            this.button1.TabIndex = 0;
            this.button1.Text = "Connect";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(574, 65);
            this.label1.TabIndex = 1;
            this.label1.Text = "Start a new Database Connection";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TB_Host
            // 
            this.TB_Host.Location = new System.Drawing.Point(288, 148);
            this.TB_Host.Name = "TB_Host";
            this.TB_Host.Size = new System.Drawing.Size(159, 31);
            this.TB_Host.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(133, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 52);
            this.label2.TabIndex = 3;
            this.label2.Text = "Host";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(133, 235);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 52);
            this.label3.TabIndex = 5;
            this.label3.Text = "Port";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TB_Port
            // 
            this.TB_Port.Location = new System.Drawing.Point(288, 246);
            this.TB_Port.Name = "TB_Port";
            this.TB_Port.Size = new System.Drawing.Size(159, 31);
            this.TB_Port.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(133, 431);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 52);
            this.label4.TabIndex = 9;
            this.label4.Text = "Password";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TB_Pass
            // 
            this.TB_Pass.Location = new System.Drawing.Point(288, 442);
            this.TB_Pass.Name = "TB_Pass";
            this.TB_Pass.Size = new System.Drawing.Size(159, 31);
            this.TB_Pass.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(133, 333);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 52);
            this.label5.TabIndex = 7;
            this.label5.Text = "User";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TB_User
            // 
            this.TB_User.Location = new System.Drawing.Point(288, 344);
            this.TB_User.Name = "TB_User";
            this.TB_User.Size = new System.Drawing.Size(159, 31);
            this.TB_User.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(133, 529);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 52);
            this.label6.TabIndex = 11;
            this.label6.Text = "DB Name";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TB_DB
            // 
            this.TB_DB.Location = new System.Drawing.Point(288, 540);
            this.TB_DB.Name = "TB_DB";
            this.TB_DB.Size = new System.Drawing.Size(159, 31);
            this.TB_DB.TabIndex = 10;
            // 
            // DBInfoPrompt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 729);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TB_DB);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TB_Pass);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TB_User);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TB_Port);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TB_Host);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(600, 800);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 800);
            this.Name = "DBInfoPrompt";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DBInfoPrompt";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TB_Host;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TB_Port;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TB_Pass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TB_User;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TB_DB;
    }
}