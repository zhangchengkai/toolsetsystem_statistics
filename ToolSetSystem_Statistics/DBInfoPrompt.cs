﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolSetSystem_Statistics
{
    public partial class DBInfoPrompt : Form
    {
        private Form1 mainForm;
        public DBInfoPrompt(Form1 mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mainForm.Host = TB_Host.Text;
            mainForm.Port = TB_Port.Text;
            mainForm.User = TB_User.Text;
            mainForm.Pass = TB_Pass.Text;
            mainForm.Dbname = TB_DB.Text;
            this.Close();
        }
    }
}
