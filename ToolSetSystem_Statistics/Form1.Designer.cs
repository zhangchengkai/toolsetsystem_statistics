﻿namespace ToolSetSystem_Statistics
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DG_Main = new System.Windows.Forms.DataGridView();
            this.MStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.LB_TBL = new System.Windows.Forms.Label();
            this.BT_Del = new System.Windows.Forms.Button();
            this.BT_Create = new System.Windows.Forms.Button();
            this.BT_Open = new System.Windows.Forms.Button();
            this.LBox_TBL = new System.Windows.Forms.ListBox();
            this.NU_Page = new System.Windows.Forms.NumericUpDown();
            this.LB_Page = new System.Windows.Forms.Label();
            this.BT_RIns = new System.Windows.Forms.Button();
            this.BT_RDel = new System.Windows.Forms.Button();
            this.LB_Total_Rows = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DG_Main)).BeginInit();
            this.MStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NU_Page)).BeginInit();
            this.SuspendLayout();
            // 
            // DG_Main
            // 
            this.DG_Main.AllowUserToAddRows = false;
            this.DG_Main.AllowUserToDeleteRows = false;
            this.DG_Main.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DG_Main.Location = new System.Drawing.Point(340, 62);
            this.DG_Main.Margin = new System.Windows.Forms.Padding(6);
            this.DG_Main.Name = "DG_Main";
            this.DG_Main.RowTemplate.Height = 23;
            this.DG_Main.Size = new System.Drawing.Size(1228, 1055);
            this.DG_Main.TabIndex = 0;
            this.DG_Main.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DG_Main_CellBeginEdit);
            this.DG_Main.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DG_Main_CellEndEdit);
            // 
            // MStrip
            // 
            this.MStrip.AutoSize = false;
            this.MStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.MStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.MStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.connectionToolStripMenuItem,
            this.connectionToolStripMenuItem1});
            this.MStrip.Location = new System.Drawing.Point(0, 0);
            this.MStrip.Name = "MStrip";
            this.MStrip.Size = new System.Drawing.Size(1568, 40);
            this.MStrip.TabIndex = 1;
            this.MStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // connectionToolStripMenuItem
            // 
            this.connectionToolStripMenuItem.Name = "connectionToolStripMenuItem";
            this.connectionToolStripMenuItem.Size = new System.Drawing.Size(67, 36);
            this.connectionToolStripMenuItem.Text = "Edit";
            // 
            // connectionToolStripMenuItem1
            // 
            this.connectionToolStripMenuItem1.Name = "connectionToolStripMenuItem1";
            this.connectionToolStripMenuItem1.Size = new System.Drawing.Size(150, 36);
            this.connectionToolStripMenuItem1.Text = "Connection";
            // 
            // LB_TBL
            // 
            this.LB_TBL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LB_TBL.Location = new System.Drawing.Point(-5, 62);
            this.LB_TBL.Name = "LB_TBL";
            this.LB_TBL.Size = new System.Drawing.Size(336, 40);
            this.LB_TBL.TabIndex = 3;
            this.LB_TBL.Text = "Tables";
            this.LB_TBL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BT_Del
            // 
            this.BT_Del.ForeColor = System.Drawing.Color.Red;
            this.BT_Del.Location = new System.Drawing.Point(8, 1105);
            this.BT_Del.Name = "BT_Del";
            this.BT_Del.Size = new System.Drawing.Size(324, 49);
            this.BT_Del.TabIndex = 4;
            this.BT_Del.Text = "Delete";
            this.BT_Del.UseVisualStyleBackColor = true;
            // 
            // BT_Create
            // 
            this.BT_Create.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BT_Create.Location = new System.Drawing.Point(7, 1050);
            this.BT_Create.Name = "BT_Create";
            this.BT_Create.Size = new System.Drawing.Size(324, 49);
            this.BT_Create.TabIndex = 5;
            this.BT_Create.Text = "Create";
            this.BT_Create.UseVisualStyleBackColor = true;
            // 
            // BT_Open
            // 
            this.BT_Open.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BT_Open.Location = new System.Drawing.Point(8, 995);
            this.BT_Open.Name = "BT_Open";
            this.BT_Open.Size = new System.Drawing.Size(324, 49);
            this.BT_Open.TabIndex = 6;
            this.BT_Open.Text = "Open";
            this.BT_Open.UseVisualStyleBackColor = true;
            this.BT_Open.Click += new System.EventHandler(this.BT_Open_Click);
            // 
            // LBox_TBL
            // 
            this.LBox_TBL.FormattingEnabled = true;
            this.LBox_TBL.ItemHeight = 25;
            this.LBox_TBL.Location = new System.Drawing.Point(12, 105);
            this.LBox_TBL.Name = "LBox_TBL";
            this.LBox_TBL.Size = new System.Drawing.Size(319, 879);
            this.LBox_TBL.TabIndex = 7;
            // 
            // NU_Page
            // 
            this.NU_Page.Location = new System.Drawing.Point(338, 1123);
            this.NU_Page.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NU_Page.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NU_Page.Name = "NU_Page";
            this.NU_Page.Size = new System.Drawing.Size(120, 31);
            this.NU_Page.TabIndex = 8;
            this.NU_Page.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NU_Page.ValueChanged += new System.EventHandler(this.NU_Page_ValueChanged);
            // 
            // LB_Page
            // 
            this.LB_Page.Location = new System.Drawing.Point(464, 1125);
            this.LB_Page.Name = "LB_Page";
            this.LB_Page.Size = new System.Drawing.Size(100, 23);
            this.LB_Page.TabIndex = 9;
            this.LB_Page.Text = "/ 1";
            this.LB_Page.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BT_RIns
            // 
            this.BT_RIns.Location = new System.Drawing.Point(570, 1120);
            this.BT_RIns.Name = "BT_RIns";
            this.BT_RIns.Size = new System.Drawing.Size(218, 37);
            this.BT_RIns.TabIndex = 10;
            this.BT_RIns.Text = "Insert Row";
            this.BT_RIns.UseVisualStyleBackColor = true;
            // 
            // BT_RDel
            // 
            this.BT_RDel.Location = new System.Drawing.Point(794, 1119);
            this.BT_RDel.Name = "BT_RDel";
            this.BT_RDel.Size = new System.Drawing.Size(218, 37);
            this.BT_RDel.TabIndex = 11;
            this.BT_RDel.Text = "Delete Row";
            this.BT_RDel.UseVisualStyleBackColor = true;
            // 
            // LB_Total_Rows
            // 
            this.LB_Total_Rows.Location = new System.Drawing.Point(1018, 1125);
            this.LB_Total_Rows.Name = "LB_Total_Rows";
            this.LB_Total_Rows.Size = new System.Drawing.Size(331, 31);
            this.LB_Total_Rows.TabIndex = 12;
            this.LB_Total_Rows.Text = "0 Record(s)";
            this.LB_Total_Rows.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1574, 1169);
            this.Controls.Add(this.LB_Total_Rows);
            this.Controls.Add(this.BT_RDel);
            this.Controls.Add(this.BT_RIns);
            this.Controls.Add(this.LB_Page);
            this.Controls.Add(this.NU_Page);
            this.Controls.Add(this.LBox_TBL);
            this.Controls.Add(this.BT_Del);
            this.Controls.Add(this.BT_Create);
            this.Controls.Add(this.BT_Open);
            this.Controls.Add(this.DG_Main);
            this.Controls.Add(this.MStrip);
            this.Controls.Add(this.LB_TBL);
            this.MainMenuStrip = this.MStrip;
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.MinimumSize = new System.Drawing.Size(1600, 1240);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ToolSetSystem_Statistics_v1.0.0";
            ((System.ComponentModel.ISupportInitialize)(this.DG_Main)).EndInit();
            this.MStrip.ResumeLayout(false);
            this.MStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NU_Page)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DG_Main;
        private System.Windows.Forms.MenuStrip MStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectionToolStripMenuItem;
        private System.Windows.Forms.Label LB_TBL;
        private System.Windows.Forms.Button BT_Del;
        private System.Windows.Forms.Button BT_Create;
        private System.Windows.Forms.Button BT_Open;
        private System.Windows.Forms.ListBox LBox_TBL;
        private System.Windows.Forms.NumericUpDown NU_Page;
        private System.Windows.Forms.Label LB_Page;
        private System.Windows.Forms.Button BT_RIns;
        private System.Windows.Forms.Button BT_RDel;
        private System.Windows.Forms.ToolStripMenuItem connectionToolStripMenuItem1;
        private System.Windows.Forms.Label LB_Total_Rows;
    }
}

