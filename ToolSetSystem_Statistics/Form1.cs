﻿using Npgsql;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections.Generic;

namespace ToolSetSystem_Statistics
{
    public partial class Form1 : Form
    {
        private NpgsqlConnection conn;
        private string currTable;
        private string host, port, user, pass, dbname;

        public string Host { get => host; set => host = value; }
        public string Port { get => port; set => port = value; }
        public string Pass { get => pass; set => pass = value; }
        public string Dbname { get => dbname; set => dbname = value; }
        public string User { get => user; set => user = value; }

        private Size ConstructSize(double x, double y)
        {
            int w = ClientSize.Width;
            int h = ClientSize.Height;
            return new Size((int)(w * x), (int)(h * y));
        }

        private Point ConstructPoint(double x, double y)
        {
            int w = ClientSize.Width;
            int h = ClientSize.Height;
            return new Point((int)(w * x), (int)(h * y));
        }

        private int recordsPerPage = 100;
        private long currPage = 1;

        private void Set_Total_Pages_And_Rows()
        {
            string sql = String.Format("SELECT COUNT(*) FROM {0}", currTable);
            try
            {
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn);
                DataSet ds = new DataSet();
                ds.Reset();
                da.Fill(ds);
                LB_Total_Rows.Text = ds.Tables[0].Rows[0]["count"].ToString() + " Record(s)";
                long numRecords = Convert.ToInt64(ds.Tables[0].Rows[0]["count"].ToString());
                long pages;
                if (numRecords % recordsPerPage != 0)
                {
                    pages = numRecords / recordsPerPage + 1;
                }
                else
                {
                    pages = numRecords / recordsPerPage;
                }
                if (pages == 0) pages = 1;
                LB_Page.Text = "/ " + pages;
                NU_Page.Maximum = pages;
            }
            catch (Exception msg)
            {
                MessageBox.Show(msg.Message);
            }
        }

        private void BT_Open_Click(object sender, EventArgs e)
        {
            currTable = (string)LBox_TBL.SelectedItem;
            Set_Total_Pages_And_Rows();
            orderCol = "id";
            isOrderAsc = true;
            NU_Page.Value = 1;
            NU_Page_ValueChanged(this, new EventArgs());
        }

        private void StartNewDBConn()
        {
            DBInfoPrompt prompt = new DBInfoPrompt(this);
            prompt.ShowDialog();

            //host = "localhost";
            //port = "5432";
            //user = "zhangchengkai";
            //pass = "654321";
            //dbname = "test";

            host = "localhost";
            port = "5432";
            user = "worker";
            pass = "123456";
            dbname = "zhack";

            try
            {
                // PostgeSQL-style connection string
                string connstring = String.Format("Server={0};Port={1};" +
                    "User Id={2};Password={3};Database={4};",
                    host, port, user,
                    pass, dbname);
                conn = new NpgsqlConnection(connstring);
                conn.Open();
                string sql = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'";
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn);
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                ds.Reset();
                da.Fill(ds);
                dt = ds.Tables[0];
                foreach(DataRow row in dt.Rows)
                {
                    LBox_TBL.Items.Add(row[0].ToString());
                }
            }
            catch (Exception msg)
            {
                MessageBox.Show(msg.Message);
                if (conn.State != ConnectionState.Closed)
                    conn.Close();
                throw;
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            ResizeControls();
        }

        private string backupValue = "";
        private void DG_Main_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            backupValue = DG_Main[e.ColumnIndex, e.RowIndex].Value.ToString();
            if(DG_Main.Columns[e.ColumnIndex].Name == "id")
                MessageBox.Show("This column is Read Only.");
        }

        private string orderCol;
        private bool isOrderAsc;
        private void NU_Page_ValueChanged(object sender, EventArgs e)
        {
            currPage = (long)NU_Page.Value;
            string sql = String.Format("SELECT * FROM {0} ORDER BY {1} {2} LIMIT 100 OFFSET {3}", currTable, orderCol, isOrderAsc ? "ASC" : "DESC", (currPage - 1) * recordsPerPage);
            try
            {
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn);
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                ds.Reset();
                da.Fill(ds);
                dt = ds.Tables[0];
                DG_Main.DataSource = dt;
            }
            catch (Exception msg)
            {
                MessageBox.Show(msg.Message);
            }
        }

        private void ResizeControls()
        {
            MStrip.Location = ConstructPoint(0, 0);
            MStrip.Size = ConstructSize(1, 0.05);
            LB_TBL.Location = ConstructPoint(0, 0.05);
            LB_TBL.Size = ConstructSize(0.25, 0.1);
            LBox_TBL.Location = ConstructPoint(0, 0.15);
            LBox_TBL.Size = ConstructSize(0.25, 0.55);
            BT_Open.Location = ConstructPoint(0, 0.7);
            BT_Open.Size = ConstructSize(0.25, 0.1);
            BT_Create.Location = ConstructPoint(0, 0.8);
            BT_Create.Size = ConstructSize(0.25, 0.1);
            BT_Del.Location = ConstructPoint(0, 0.9);
            BT_Del.Size = ConstructSize(0.25, 0.1);
            DG_Main.Location = ConstructPoint(0.25, 0.05);
            DG_Main.Size = ConstructSize(0.75, 0.90);
            int NU_height = NU_Page.Height;
            int NU_DesiredHeight = ConstructSize(0.15, 0.05).Height;
            NU_Page.Location = new Point(ConstructPoint(0.25, 0.95).X, ConstructPoint(0.25,0.95).Y+(NU_DesiredHeight - NU_height) / 2);
            NU_Page.Size = new Size(ConstructSize(0.15, 0.05).Width, NU_height);
            LB_Page.Location = ConstructPoint(0.4, 0.95);
            LB_Page.Size = ConstructSize(0.1, 0.05);
            BT_RIns.Location = ConstructPoint(0.5, 0.95);
            BT_RIns.Size = ConstructSize(0.15, 0.05);
            BT_RDel.Location = ConstructPoint(0.65, 0.95);
            BT_RDel.Size = ConstructSize(0.15, 0.05);
            LB_Total_Rows.Location = ConstructPoint(0.8, 0.95);
            LB_Total_Rows.Size = ConstructSize(0.2, 0.05);
        }

        private void Form1_FormClosed(Object sender, FormClosedEventArgs e)
        {
            if (conn.State != ConnectionState.Closed)
                conn.Close();
        }

        private void DG_Main_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            string colName = DG_Main.Columns[e.ColumnIndex].Name;
            if(colName == "id")
            {
                DG_Main[e.ColumnIndex, e.RowIndex].Value = backupValue;
                return;
            }
            string dataAfterChange = DG_Main[e.ColumnIndex, e.RowIndex].Value.ToString();
            string recordId = DG_Main[0, e.RowIndex].Value.ToString();

            string sql;
            if (dataAfterChange == "") 
            {
                sql = String.Format("UPDATE {0} SET {1} = NULL WHERE id = {2}", currTable, colName, recordId);
            }
            else
            {
                sql = String.Format("UPDATE {0} SET {1} = '{2}' WHERE id = {3}", currTable, colName, dataAfterChange, recordId);
            }

            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception msg)
            {
                MessageBox.Show(msg.Message);
                DG_Main[e.ColumnIndex, e.RowIndex].Value = backupValue;
            }
        }

        public Form1()
        {
            InitializeComponent();
            Resize += Form1_Resize;
            FormClosed += Form1_FormClosed;
            ResizeControls();
            StartNewDBConn();
        }
    }
}
